Role AUTOFS
=========

The purpose of this role is to allow Administrator to automate the configuration of automaps to allow automatic mounts of CIFS shares.  This role can also be adapted and extended for other shares  NFSv4 etc.

Compatiblity
------------

This role can be used only on Ubuntu 18.04  LTS and later versions.  During the test phase we noted one issue with the format of the Kerberos ticket which gave an error -126 (CIFS VFS: Send error in SessSetup = -126) when a shared folder was listed by an end-user.

```
Apr  3 11:26:03 stivm0025 cifs.upcall: get_cachename_from_process_env: pathname=/proc/10284/environ
Apr  3 11:26:03 stivm0025 cifs.upcall: get_existing_cc: default ccache is FILE:/tmp/krb5cc_79738
Apr  3 11:26:03 stivm0025 cifs.upcall: get_tgt_time: unable to get principal

```

If you use this role on an already kerberised Ubuntu, you may need to change only the /etc/sssd/sssd.conf file and force the kerberos ticket format by adding this line in your file,

```
krb5_ccname_template=FILE:%d/krb5cc_%U
```

If you use our krbsso as well, you will need to copy an extra file in /etc/profile.d/ for automount to work.  This is done in the autofs role.

Summary of the role variables used
--------------

You can find in vars/main.yml variables used to installed the service.  Other variables are used in the automount.j2 template.  When installing the role, it is mandatory to add the following to your command

```
--extra-vars "nas_server_fqdn=Fully Qualified Domain name of your CIFS share unit_share=name of your share"
E.g. ansible-playbook -i ../host playbooks/automount.yml  --extra-vars "nas_server_fqdn=stisrv.intranet.epfl.ch unit_share=sti-it"   -u root
```



| Variable              | Default Value | Type   | Description                  |
| --------------------- | :------------ | :----- | ---------------------------- |
| autofs_mount_packages | Autofs        | String | Packages to install for role |
| nas_server_fqdn       | Required      | String | Fully qualified domain name  |
| unit_share            | Required      | String | Name of share                |

Dependencies
------------

This role requires the krbsso role to run first.  If you have an Ubuntu machined already joined to a AD Domain you may use the Autofs role on its own with the extra vars (see variable used for details)

Example Playbook
----------------

    - hosts: servers
      roles:
         - ../roles/autofs

## Source and Copyright:

Part of this role has been inspired by other autofs roles found on Ansible Galaxy web site.  However some parts had to be rewritten to be used in our infrastructure. 







